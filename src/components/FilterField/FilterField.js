import React, { Component } from 'react';
import { FormGroup, FormControl } from 'react-bootstrap';
import './FilterField.css';

export default class FilterField extends Component {

    _handleSearchChange = (searchInput) => {  
        this.props.onChange(searchInput.currentTarget.value);
    }

    render() {
        return <FormGroup className = 'filterField'> 
                    <FormControl
                            type = 'text'
                            name = 'search'
                            bsClass = 'search'
                            id = 'search' 
                            placeholder = 'Search...'
                            onChange= {this._handleSearchChange} />    
                </FormGroup>
    }
}
