import React, {Component} from 'react';
import { createStore } from 'redux';
import MyFunction from '../redux/modules/users';
import { initialState } from '../redux/modules/users';
import { Provider } from 'react-redux';
import LoginFormContainer from './LoginFormContainer/LoginFormContainer';
import UsersInfo from './UsersInfo/UsersInfo';

const store = createStore(
    MyFunction,
    initialState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);


class App extends Component {
      
    render() {
        return (
            <Provider store={store}>
                <div>
                    <LoginFormContainer />
                    <UsersInfo/>
                </div>
            </Provider>
        )
    };
};

export default App;