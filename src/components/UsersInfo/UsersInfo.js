import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actions } from '../../redux/modules/users';
import moment from 'moment';
import './UsersInfo.css';
import Button from '../Button/Button';
import FilterField from '../FilterField/FilterField';

const mapStateToProps = (state) => { 
    return {
        users: state.users,
        currentUser: state.currentUser
    }
};
  
const mapActionsToProps = {
    removeUsersData: actions.removeUsersData,
    editUsersData: actions.editUsersData,
};

class UsersInfo extends Component {    
    constructor(props) {
        super(props);
        this.props = props;
        this.state = {
            search: '',
            visibility: 'none'
        }
    
    }
    _getSearchValue = (searchValue) => {
        this.setState({
            search: searchValue
        })
    }

    _makeActionWithUsersData = (idToDo, actionToDo) => {
        actionToDo === "delete" && this.props.removeUsersData(idToDo);
        actionToDo === "edit" && this.props.editUsersData(idToDo);    
    }

    render(){ 
        const usersInfo = this.props.users.filter(user => 
           user.login.includes(this.state.search) || user.phone.includes(this.state.search)
            ).map((user, index) => 
            <div className = 'usersInfoContainer'
                key = {user.id}>
                <p className = 'index'>
                    {(index + 1) + ':'}
                </p>
                <p className = 'column'>
                    {'Login: ' + user.login + ';'}
                </p>
                <p className = 'column'>
                    {'Password: ' + user.password + ';'}
                </p>
                <p className = 'column'>
                    {'Phone: ' + user.phone + ';'}
                </p>
                <p className = 'column'>
                    {'Date: ' + user.startDate.format('DD/MM/YYYY') + ';'}
                </p>
                <Button
                    index = {user.id}
                    makeActionWithUser = {this._makeActionWithUsersData}
                    action = 'delete'>
                    X
                </Button>
                <Button
                    index = {user.id}
                    makeActionWithUser = {this._makeActionWithUsersData}
                    action = 'edit'>
                    ?
                </Button>
            </div> )
            
        return(
            <div className = 'usersInfo'>
                <FilterField onChange = {this._getSearchValue}/>
                {usersInfo}
            </div>
        )
    }
}

export default connect(mapStateToProps, mapActionsToProps)(UsersInfo);