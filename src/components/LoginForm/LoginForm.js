import React, { Component } from 'react';
import { FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import './LoginForm.css';

export class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.props = props;
        this.state = {
            id: '',
            login: '',
            password: '',
            phone: '',
            startDate: moment(),
            textContent: 'Submit',
            visibility: 'none'
        }
    }
    
    _handleInputChange = (event) => {
        event.currentTarget.name === "login" && this.setState({login: event.currentTarget.value});
        event.currentTarget.name === "password" && this.setState({password: event.currentTarget.value}); 
        event.currentTarget.name === "phone" && this.setState({phone: event.currentTarget.value});  
    } 

    _handleDateChange = (date) => {
        this.setState({startDate: date}); 
    }
    
    _handleSubmit = (event) => {
        (this.state.login.length === 0)
        ? alert ('Enter login!')
        : (this.state.login.indexOf(' ') > -1)
        ? alert ('Enter login without spaces')
        : (this.state.password.length === 0)
        ? alert ('Enter password!')
        : (this.state.password.length < 8)
        ? alert ('Your password have to be minimum 8 symbols!')
        : (this.state.password.indexOf(' ') > -1)
        ? alert ('Enter password without spaces')
        : ((/^\d{10,}$/).test(this.state.phone) === false)
        ? alert ('Enter your true phone using only numbers')
        : this.props.onSubmit({
            id: this.state.id,
            login: this.state.login,
            password: this.state.password,
            phone: this.state.phone,
            startDate: this.state.startDate
        });
        event.preventDefault();        
        this.setState({
            login: '',
            password: '',
            phone: '',
            startDate: moment()
        }); 
    }

    componentWillReceiveProps = (nextProps) => {
        nextProps.currentUser !== undefined
        ? this.setState({
            id: nextProps.currentUser.id,
            login: nextProps.currentUser.login,
            password: nextProps.currentUser.password,
            phone: nextProps.currentUser.phone,
            startDate: nextProps.currentUser.startDate,
            textContent: 'Edit',
            visibility: 'inline-block'
        })
        : this.setState({
            id: '',
            login: '',
            password: '',
            phone: '',
            startDate: moment(),
            textContent: 'Submit',
            visibility: 'none'
        });
    }

    render() {
        return (
            <FormGroup className = 'form'>
                <ControlLabel 
                        bsClass = 'label'
                        htmlFor = 'login'>
                Login:
                </ControlLabel>
                <FormControl 
                        type = 'login'
                        name = 'login'
                        bsClass = 'input'
                        id = 'login' 
                        placeholder = 'login'
                        value={this.state.login}
                        onChange={this._handleInputChange} />
                
                <ControlLabel 
                        bsClass = 'label'
                        htmlFor = 'password'>
                Password:
                </ControlLabel>
                <FormControl 
                        type = 'password' 
                        name = 'password'
                        bsClass = 'input'
                        id = 'password' 
                        placeholder = 'password'
                        value={this.state.password}
                        onChange={this._handleInputChange} />
                <ControlLabel 
                        bsClass = 'label'
                        htmlFor = 'phone'>
                Phone:
                </ControlLabel>
                <FormControl 
                        type = 'phone'
                        name = 'phone'
                        id = 'phone'
                        placeholder = 'phone'
                        bsClass = 'input'
                        value = {this.state.phone}
                        onChange={this._handleInputChange} />
                <ControlLabel 
                        bsClass = 'label'
                        htmlFor = 'date'>
                Date:
                </ControlLabel>
                <DatePicker 
                        id = 'date'
                        name = 'date'
                        withPortal
                        showMonthDropdown
                        showYearDropdown
                        dropdownMode="select"
                        selected={this.state.startDate}
                        onChange={this._handleDateChange}
                        dateFormat="DD/MM/YYYY"
                        />
                <Button 
                        onClick={this._handleSubmit}
                        bsClass = 'submit'
                        type = 'submit'>
                {this.state.textContent}
                </ Button>
                <Button style = {{display: this.state.visibility}}
                        onClick={this.props.onCancel}
                        bsClass = 'submit'>
                Cancel
                </ Button>
            </FormGroup>
        )
    }
}