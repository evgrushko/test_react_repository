import React, { Component } from 'react';
import { Counter } from '../Counter/Counter';
import { connect } from 'react-redux';
import { actions } from '../../redux/modules/users';
import { LoginForm } from '../LoginForm/LoginForm';

const uuid = require('uuid-v4');

const mapStateToProps = (state) => { 
    return {
        users: state.users,
        currentUser: state.currentUser
    }
};
  
const mapActionsToProps = {
    changeCounterValue: actions.changeCounterValue,
    editCurrentUserValue: actions.editCurrentUserValue,
    cancelCurrentUserChages: actions.cancelCurrentUserChages
};

class LoginFormContainer extends Component {
    _getUsersData = (inputData) => {
        if (this.props.currentUser === undefined) {
            inputData.id = uuid();
            this.props.changeCounterValue(inputData);
            alert('Added new user: ' + inputData.login);
        } else {
            this.props.editCurrentUserValue(inputData);
        }        
    }

    _cancelUserChanges = () => {
        this.props.cancelCurrentUserChages();
    }

    render() {
        return(
            <div>
                <Counter UsersLength = {this.props.users.length} />
                <LoginForm onSubmit = {this._getUsersData} 
                           onCancel = {this._cancelUserChanges}
                           currentUser = {this.props.currentUser}/>
            </div>
        )
    }
};

export default connect(mapStateToProps, mapActionsToProps)(LoginFormContainer);