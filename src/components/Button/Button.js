import React, {Component} from 'react';
import './Button.css';

export default class Button extends Component {
    _makeActionWithUser = (elem) => {
        this.props.makeActionWithUser(elem.currentTarget.id, elem.currentTarget.name);
    }

    render(){
        return (
            <button id = {this.props.index}
                    className = "buttonCounter"
                    name = {this.props.action}
                    onClick = {this._makeActionWithUser}>
                {this.props.children}
            </button>        
        )    
    }
}
