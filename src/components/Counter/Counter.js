import React, {Component} from 'react';
import './Counter.css';

export class Counter extends Component {
    constructor(props) {
        super(props);
        this.props = props;
        this.state = {
            count: [0, 0, 0]
        }
    }
    
    componentWillReceiveProps = (nextProps) => {
        let numOfUsers = nextProps.UsersLength;
        let arrOfUsers = String(numOfUsers).split('');
        if (arrOfUsers.length < 3) {
            for (let i = arrOfUsers.length; i < 3; i++) {
                arrOfUsers.unshift('0');
            }
        }
        this.setState({
            count: arrOfUsers
        })
    }
    
    render() {
        const counterValue = 
        this.state.count.map((elem, index) =>
            <div key = {index}
                className = 'counterElem'>
                {elem}
            </div> ) 
        return (
            <div className = 'container'>
                {counterValue}             
            </div>  
        );
        }   
    };

