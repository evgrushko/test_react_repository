export const initialState = {
    users: [],
    currentUser: undefined
};

export const actions = {
  changeCounterValue: function(users) {
    return { 
      type: 'CHANGE_COUNTER_VALUE',
      payload: users 
    }
  },
  removeUsersData: function(users) {
    return {
      type: 'REMOVE_USERS_DATA',
      payload: users
    }
  },
  editUsersData: function(users) {
    return {
      type: 'EDIT_USERS_DATA',
      payload: users
    }
  },
  editCurrentUserValue: function(users) {
    return {
      type: 'EDIT_CURRENT_USER_VALUE',
      payload: users
    }
  },
  cancelCurrentUserChages: function(users) {
    return {
      type: 'CANCEL_CURRENT_USER_CHANGES',
      payload: users
    }
  }
};
  
export default function (state = initialState, action) {
  switch (action.type) {
    case 'CHANGE_COUNTER_VALUE':
      return {
        ...state,
        users: [...state.users, action.payload]
      }
    case 'REMOVE_USERS_DATA':
      return {
        ...state,
        users: state.users.filter((users) => 
          users.id !== action.payload
        )
      }
      case 'EDIT_USERS_DATA':
      return {
        ...state,
        currentUser: state.users.find((users) => users.id === action.payload)
      }
      case 'EDIT_CURRENT_USER_VALUE':  
      return {
        ...state,
        users: state.users.map(users => 
          users.id === action.payload.id
          ? action.payload
          : users
        ),
        currentUser: undefined
      }
      case 'CANCEL_CURRENT_USER_CHANGES':
      return {
        ...state,
        currentUser: undefined
      }
    default:
      return state;
  }
}
